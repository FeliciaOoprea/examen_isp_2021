public class Subiectul1 {
    public static void main(String[] args) {

    }
}

interface C {
}

class A  {
    public void met(B b) { //dependenta
    }
}

class B {
    private long t;
    D d; //agregare
    E e; //asociere

    public B(long t, D d, E e) {
        this.t = t;
        this.d = d;
        this.e = e;
    }

    public void x() {
    }
}

class D {
}

class E implements C{
    F f = new F(); //compozitie
    public void metG(int i) {
    }
}

class F {
    public void metA() {
    }
}
