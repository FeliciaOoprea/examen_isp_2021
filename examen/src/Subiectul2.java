import javax.swing.*;

public class Subiectul2 extends JFrame {
    private JTextField textField;

    public Subiectul2() {
        setTitle("Examen_isp_Subiectul2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(300, 150);
        setResizable(false);
        init();
        this.repaint();
    }

    public void init() {
        this.setLayout(null);
        textField = new JTextField();
        textField.setBounds(20, 20, 250, 50);
        add(textField);

        JButton button = new JButton("Write in console!");
        button.setBounds(20, 80, 250, 20);
        add(button);
        button.addActionListener(e -> System.out.println(textField.getText()));
    }

    public static void main(String[] args) {
        new Subiectul2();
    }
}
